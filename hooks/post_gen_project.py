import os
from os import path
import shutil
import sys

p = path.abspath(path.join(__file__ ,"../.."))
tempFolder = p + "\\{{cookiecutter.suiteName}}\\templates"

shutil.rmtree(tempFolder, ignore_errors=True)
sys.exit(0)