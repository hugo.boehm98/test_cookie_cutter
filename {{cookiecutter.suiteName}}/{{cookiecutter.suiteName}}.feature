Feature: {{cookiecutter.suiteName}}
{% for use_case, details in cookiecutter.useCases|dictsort %}

	{%- for key in details.keyword[2:] if key.stereotype == "ST_BPMN_MANUAL_TASK_2" -%}
		{% if key.name is belongsToCategory("VALIDATION") %}
			{{key.documentation | replace("#", "@")}}
		{%- endif -%}
	{% endfor %}
	Scenario: {{details.name}}  	
	{%- for key in details.keyword[0:] %}		
		{%- if key.type == "OT_FUNC" and not key.name is ValidationKeyword and not loop.previtem.type == "OT_FUNC" %} 
			Given {{key.name}}
		{%- elif key.type == "OT_FUNC" and not key.name is ValidationKeyword and loop.previtem.type == "OT_FUNC" %} 
			And {{key.name}}
		{%- elif key.type == "CONNECTOR" and not loop.previtem.type == "CONNECTOR" %}
			When {{key.name}}
		{%- elif key.type == "CONNECTOR" and loop.previtem.type == "CONNECTOR" %} 
			And {{key.name}}
		{%- elif key.type == "OT_FUNC" and key.name is ValidationKeyword and not loop.previtem.type == "OT_FUNC" %} 
			Then {{key.name}}
		{%- elif key.type == "OT_FUNC" and key.name is ValidationKeyword and loop.previtem.type == "OT_FUNC" %} 
			And {{key.name}}
		{%- endif -%}	
	{%- endfor %}	
{% endfor %}

Generate with Cookie Cutter